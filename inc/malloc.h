/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 07:27:00 by mlalisse          #+#    #+#             */
/*   Updated: 2014/04/23 00:49:23 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <stdlib.h>

# define ALIGN(mem) (mem & f ? ((mem >> 4) + 1) << 4 : mem)

# define CHUNK_SIZE(chunk) ((*((size_t *) chunk)) >> 1)
# define CHUNK_INUSE(chunk) ((*((size_t *) chunk)) & 1)
# define CHUNK_FREE(chunk) (!CHUNK_INUSE(chunk))
# define CHUNK_END(c) (c + CHUNK_SIZE(c) + sizeof(size_t))

typedef struct	s_malloc {
	void   *tiny;
	void   *small;
	void   *medium;
	void   *large;
}				t_malloc;

void	*ft_malloc(size_t size);

void	*ft_realloc(void *ptr, size_t size);

void	ft_free(void *ptr);

void	show_alloc_mem();

#endif

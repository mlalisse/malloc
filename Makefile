CC = gcc
CFLAGS = -Wall -Wextra -I inc -I libft/inc -L libft -lft -fPIC -g

INC = inc/malloc.h

SRC_NAME = malloc.c main.c
SRC = $(addprefix src/, $(SRC_NAME))
LIB = $(addprefix obj/, $(SRC_NAME:.c=.o))

NAME = malloc

# HOSTTYPE = $(shell uname -m)

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

all: $(LIB) libft/libft.a
	$(CC) $(CFLAGS) $(LIB) -o $(NAME)

clean:
	rm -rf $(LIB)

re: clean all

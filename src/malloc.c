/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 07:31:10 by mlalisse          #+#    #+#             */
/*   Updated: 2014/04/23 00:51:05 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <sys/mman.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "malloc.h"

t_malloc  g_malloc = {NULL, NULL, NULL, NULL};

void	set_head(void *chunk, size_t head)
{
	*((size_t *) chunk) = head;
}

void	set_chunk(void *mem, size_t head)
{
	set_head(mem, head);
	set_head(CHUNK_END(mem), head);
}

void	*go_to_next_chunk(void *chunk)
{
	return (chunk + CHUNK_SIZE(chunk) + 2 * sizeof(size_t));
}

void	*go_to_prev_chunk(void *chunk)
{
	if (*((size_t *) (chunk - sizeof(size_t))) == 0) // ?
		return (NULL);
	return (chunk - CHUNK_SIZE(chunk - sizeof(size_t)) - 2 * sizeof(size_t));
}


void	*ft_malloc_init_pool(size_t size)
{
	void	*mem;

	mem = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
	if (mem == MAP_FAILED || mem == NULL)
		return (NULL);
	set_head(mem, 0);
	set_chunk(mem + sizeof(size_t), (size - 5 * sizeof(size_t)) << 1);
	set_head(CHUNK_END(mem) + 2 * sizeof(size_t), 0);
	set_head(CHUNK_END(mem) + 3 * sizeof(size_t), 0);
	return (mem + sizeof(size_t));
}

void	*ft_malloc_pool(size_t size, void **pool, size_t pool_size)
{
	void	*chunk;
	size_t	next_size;

	if (*pool == NULL)
		*pool = ft_malloc_init_pool(pool_size);
	chunk = *pool;

	// if -- Liste chainee de suivants
	// while (chunk && CHUNK_SIZE(chunk) < size)
	// {
	//   if (!NEXT_CHUNK(chunk))
	//   	set_next_chunk(chunk, ft_malloc_init_pool(pool_size));
	//   chunk = NEXT_CHUNK(chunk)
	//  }
	// fi --

	if (chunk == NULL)
		return (NULL);

	// else --
	while (*((size_t *) chunk) != 0 && (CHUNK_INUSE(chunk) || CHUNK_SIZE(chunk) < size))
		chunk = go_to_next_chunk(chunk);

	if (*((size_t *) chunk) == 0)
		return (ft_malloc_pool(size, chunk + sizeof(size_t), pool_size));
	// -- else

	next_size = CHUNK_SIZE(chunk) - size - 2 * sizeof(size_t);
	if (next_size < 2 * sizeof(void *))
	{
		set_chunk(chunk, (CHUNK_SIZE(chunk) << 1) + 1);
		// remove l'elem de free_chunk list
	}
	else
	{
		set_chunk(chunk, (size << 1) + 1);
		set_chunk(go_to_next_chunk(chunk), next_size << 1);
		// replace l'elem de free_chunk list
	}

	return (chunk + sizeof(size_t));
}

// TODO: Handle large malloc !! (db linked list)
void	*ft_malloc_large(size_t size)
{
	void	*chunk;

	size += sizeof(size_t) + 2 * sizeof(void *);
	// rendre multiple of 4096
	chunk = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	if (chunk == NULL)
		return (NULL);
	set_head(chunk, 0);
	if (g_malloc.large)
		set_head(g_malloc.large, (size_t) chunk);
	set_head(chunk + 1 * sizeof(void *), (size_t) g_malloc.large);
	set_head(chunk + 2 * sizeof(void *), (size << 1) + 1);
	g_malloc.large = chunk;
	return (chunk + 2 * sizeof(void *) + sizeof(size_t));
}

# define MIN_CHUNK_SIZE (2 * sizeof(void *) + 2 * sizeof(size_t))
void	*ft_malloc(size_t size)
{
	size = (size < 32) ? 32 : size;
	// align size
	if (size <= 512 - 2 * sizeof(size_t))
		return (ft_malloc_pool(size, &g_malloc.tiny, 8 * 4096));
	else if (size <= 4096 - 2 * sizeof(size_t))
		return (ft_malloc_pool(size, &g_malloc.small, 64 * 4096));
	else if (size <= 16 * 4096 - 2 * sizeof(size_t))
		return (ft_malloc_pool(size, &g_malloc.medium, 512 * 4096));
	else
		return (ft_malloc_large(size));
}

void	*ft_realloc(void *ptr, size_t new_size)
{
	void	*chunk;
	void	*next_chunk;
	size_t	next_chunk_size;
	size_t	available_size;
	size_t	size;

	new_size = (new_size < 32) ? 32 : new_size; // ALIGN

	if (ptr == NULL)
		return ft_malloc(new_size);
	chunk = ptr - sizeof(size_t);
	size = CHUNK_SIZE(chunk);
	if (new_size == size)
		return (ptr);

	next_chunk = go_to_next_chunk(chunk);
	if (((size_t*) next_chunk) != 0 && CHUNK_FREE(next_chunk))
		next_chunk_size = CHUNK_SIZE(next_chunk) + 2 * sizeof(size_t);
	else
		next_chunk_size = 0;

	// Si il suit un chunk free (suffisament grand)
	if (new_size < size || (next_chunk_size && new_size <= size + next_chunk_size))
	{
		available_size = size + next_chunk_size;
		if (available_size - new_size <= 2 * (sizeof(void *) + sizeof(size_t)))
			set_chunk(chunk, (available_size << 1) + 1);
		else
		{
			set_chunk(chunk, (new_size << 1) + 1);
			next_chunk = go_to_next_chunk(chunk);
			set_chunk(next_chunk, (available_size - new_size - 2 * sizeof(size_t)) << 1);
		}
		return (ptr);
	}
	else
	{
		void *new_ptr = ft_malloc(new_size);
		memcpy(new_ptr, ptr, size); // HORS NORME 42
		ft_free(ptr);
		return (new_ptr);
	}
}

void	ft_free_pool(void *ptr)
{
	void	*chunk;
	void	*next_chunk;
	void	*prev_chunk;
	size_t	size;

	if (ptr == NULL)
		return ;

	chunk = ptr - sizeof(size_t);
	size = CHUNK_SIZE(chunk);

	next_chunk = go_to_next_chunk(chunk);
	prev_chunk = go_to_prev_chunk(chunk);

	// -- CONCAT
	if (next_chunk && CHUNK_FREE(next_chunk))
		size += CHUNK_SIZE(next_chunk) + 2 * sizeof(size_t);
	if (prev_chunk && CHUNK_FREE(prev_chunk))
	{
		size += CHUNK_SIZE(prev_chunk) + 2 * sizeof(size_t);
		chunk = prev_chunk;
	}
	// -- !CONCAT

	set_head(chunk, size << 1);
	//munmap(chunk, CHUNK_SIZE(chunk)); if BIG
}

void	ft_free_large(void *ptr)
{
	void	*chunk;
	void	*chunk2;

	chunk = ptr - 2 * sizeof(void *) - sizeof(size_t);
	if (*((void **) chunk)) // prev
	{
		chunk2 = *((void **) chunk);
		set_head(chunk2 + 1 * sizeof(void *), *((size_t *) (chunk + 1 * sizeof(void *))));
	}
	else 
		g_malloc.large = *((void **) (chunk + 1 * sizeof(void *)));
	if (*((void **) (chunk + 1 * sizeof(void *))))
	{
		chunk2 = *((void **) (chunk + 1 * sizeof(void *)));
		set_head(chunk2, *((size_t *) (chunk)));
	}
	munmap(chunk, CHUNK_SIZE(chunk + 2 * sizeof(void *)));
}

void	ft_free(void *ptr)
{
	void	*chunk;
	size_t	size;

	if (ptr == NULL)
		return ;

	chunk = ptr - sizeof(size_t);

	size = CHUNK_SIZE(chunk);

	if (size <= 512 - 2 * sizeof(size_t))
		ft_free_pool(ptr);
	else
		ft_free_large(ptr);
}

void	malloc_debug()
{
	void	*chunk;

	chunk = g_malloc.tiny;
	printf("tiny: %p\n", g_malloc.tiny);
	while (chunk && (*((size_t*) chunk) != 0 || *((size_t*) (chunk + sizeof(size_t))) != 0))
	{
		if (*((size_t*) chunk) == 0 && *((size_t*) (chunk + sizeof(size_t))) != 0)
		{
			chunk = *((void **) (chunk + sizeof(size_t)));
			printf("next pages: %p\n", chunk);
		}
		printf("(%lu) %p - %p : %lu octets\n", CHUNK_INUSE(chunk), chunk + sizeof(size_t),
			chunk + sizeof(size_t) + CHUNK_SIZE(chunk), CHUNK_SIZE(chunk));
		chunk = go_to_next_chunk(chunk);
	}
	chunk = g_malloc.small;
	printf("small: %p\n", g_malloc.small);
	while (chunk && *((size_t*) chunk) != 0)
	{
		printf("(%lu) %p - %p : %lu octets\n", CHUNK_INUSE(chunk), chunk + sizeof(size_t),
			chunk + sizeof(size_t) + CHUNK_SIZE(chunk), CHUNK_SIZE(chunk));
		chunk = go_to_next_chunk(chunk);
	}
	chunk = g_malloc.medium;
	printf("medium: %p\n", g_malloc.medium);
	while (chunk && *((size_t*) chunk) != 0)
	{
		printf("(%lu) %p - %p : %lu octets\n", CHUNK_INUSE(chunk), chunk + sizeof(size_t),
			chunk + sizeof(size_t) + CHUNK_SIZE(chunk), CHUNK_SIZE(chunk));
		chunk = go_to_next_chunk(chunk);
	}
	chunk = g_malloc.large;
	printf("large: %p\n", g_malloc.large);
	while (chunk)
	{
		void *chunk_head = chunk + 2 * sizeof(void *);
		printf("(%lu) %p - %p : %lu octets\n", CHUNK_INUSE(chunk_head), chunk_head + sizeof(size_t),
			chunk_head + sizeof(size_t) + CHUNK_SIZE(chunk_head), CHUNK_SIZE(chunk_head));
		chunk = *((void **) (chunk + 1 * sizeof(void *)));
	}

	/*
	chunk = g_malloc.large;
	ft_putendl("LARGE : ");
	while (chunk)
	{
		ft_putnbr((int) chunk); // hex
		ft_putstr(" - ");
		ft_putnbr((int) ((char*)chunk + CHUNK_SIZE(chunk))); // hex
		ft_putstr(" : ");
		ft_putnbr((int) CHUNK_SIZE(chunk));
		ft_putendl(" octets");
		list = list->next;
	}
	*/
}

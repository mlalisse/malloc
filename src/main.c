/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 07:38:13 by mlalisse          #+#    #+#             */
/*   Updated: 2014/04/22 03:10:35 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "assert.h"

int		main()
{
	int		i;
	int		*tab;
	int		*tab2;
	int		*tab3;

	ft_malloc(600);
	ft_malloc(15 * 4096);

	tab = ft_malloc(17 * 4096);
	tab2 = ft_malloc(65555);
	malloc_debug(); ft_putendl("");

	ft_free(tab);
	malloc_debug(); ft_putendl("");

	tab3 = ft_malloc(222222222);
	malloc_debug(); ft_putendl("");

	ft_free(tab3);
	malloc_debug(); ft_putendl("");

	ft_free(tab2);
	malloc_debug(); ft_putendl("");

	tab = ft_malloc(1111111);
	malloc_debug(); ft_putendl("");

	tab2 = ft_malloc(2222222);
	malloc_debug(); ft_putendl("");

	tab3 = ft_malloc(3333333);
	malloc_debug(); ft_putendl("");

	ft_free(tab2);
	malloc_debug(); ft_putendl("");

	ft_free(tab);
	malloc_debug(); ft_putendl("");

	ft_free(tab3);
	malloc_debug(); ft_putendl("");

	/*
	tab2 = ft_malloc(100);

	tab3 = ft_malloc(256);

	malloc_debug(); ft_putendl("");

	ft_free(tab3);
	malloc_debug(); ft_putendl("");

	tab3 = ft_malloc(256);
	malloc_debug(); ft_putendl("");

	ft_free(tab2);
	malloc_debug(); ft_putendl("");

	ft_free(tab);
	malloc_debug(); ft_putendl("");

	tab = ft_malloc(80);
	malloc_debug(); ft_putendl("");

	tab = ft_malloc(8000);
	tab2 = ft_malloc(45000);
	tab3 = ft_malloc(6000);
	malloc_debug(); ft_putendl("");

	ft_free(tab2);
	ft_malloc(4500);
	ft_malloc(4500);
	ft_malloc(7500);
	ft_malloc(7500);

	malloc_debug(); ft_putendl("");

	free(NULL);
	*/

	return (0);
}
